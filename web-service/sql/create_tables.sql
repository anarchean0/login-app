create table if not exists users(
  id serial primary key,
  name varchar(256) not null unique,
  pass_hash varchar(128) not null,
  created_at timestamp with time zone not null default now(),
  updated_at timestamp with time zone not null default now()
);

create or replace function update_updated_at_users()
returns trigger as $$
begin
  NEW.updated_at = now();
  return NEW;
end;
$$ language 'plpgsql';

create trigger if not exists on_users_update_update_time
  before update on users for each row execute procedure update_updated_at_users();
