<?php

namespace LoginApp\Util;

function mod($a, $n) {
    return $a - $n * floor($a / $n);
}
