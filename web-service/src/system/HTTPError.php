<?php

namespace LoginApp\System;

require_once __ROOT__ . "/system/HTTPResponse.php";

class HTTPError extends \Exception {
  private $errors;

  public function __construct($errors, $code = 400) {
    $this->errors = $errors;
    $this->code = $code;

    if (is_string($this->errors)) {
      $this->errors = array($this->errors);
    }
  }

  public function makeJSONResponse() {
    $r = new HTTPResponse();
    $r->setStatus($this->code);
    $r->setBody(array(
      "errors" => $this->errors
    ));

    return $r;
  }
}
