<?php

namespace LoginApp\System;

class Route {
  private $path;
  private $methods;
  private $action;

  const HTTP_METHODS = array('GET', 'POST', 'PUT', 'PATCH', 'DELETE',
      'HEAD', 'OPTIONS');

  public function __construct($path, $action, $methods = FALSE) {
    $this->path = $path;
    $this->action = $action;
    $this->methods = $methods;

    if (!$this->methods) {
      $this->methods = self::HTTP_METHODS;
    }

    if (is_string($this->methods)) {
      $this->methods = explode(",", $this->methods);
    }
  }

  public function match($request) {
    if ($request->getPath() == $this->path
        && in_array($request->getMethod(), $this->methods)) {
      return $this->action;
    }

    return null;
  }
}
