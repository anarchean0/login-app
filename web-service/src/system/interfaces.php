<?php

namespace LoginApp\System;

interface Filter {
  function handleRequest($request);
}
