<?php

namespace LoginApp\Models;

class User
{
  private $id;
  private $name;
  private $pass_hash;
  private $created_at;
  private $updated_at;
  private $dirty;

  public function __construct($name = null, $pass_hash = null,
      $created_at = null, $updated_at = null, $id = null) {
    $this->id = $id;
    $this->name = $name;
    $this->pass_hash = $pass_hash;
    $this->created_at = $created_at;
    $this->updated_at = $updated_at;
    $this->dirty = $id ? FALSE : TRUE;
  }

  public function get_id() {
    return $this->id;
  }

  public function set_id($id) {
    $this->id = $id;
    $this->dirty = TRUE;
  }

  public function get_name() {
    return $this->name;
  }

  public function set_name($name) {
    $this->name = name;
    $this->dirty = TRUE;
  }

  public function get_pass_hash() {
    return $this->pass_hash;
  }

  public function set_pass_hash($pass_hash) {
    $this->pass_hash = $pass_hash;
    $this->dirty = TRUE;
  }

  public function get_created_at() {
    return $this->created_at;
  }

  public function set_created_at($created_at) {
    $this->created_at = $created_at;
    $this->dirty = TRUE;
  }

  public function get_updated_at() {
    return $this->updated_at;
  }

  public function set_updated_at($updated_at) {
    $this->updated_at = $updated_at;
    $this->dirty = TRUE;
  }

  public function set_password($password) {
    $this->pass_hash = password_hash($password, PASSWORD_ARGON2I);
    $this->dirty = TRUE;
  }

  public function verify_password($password) {
    if (password_verify($password, $this->pass_hash)) {
      if (password_needs_rehash($this->pass_hash, PASSWORD_ARGON2I)) {
        $this->set_password($password);
      }

      return TRUE;
    }

    return FALSE;
  }

  public function is_dirty() {
    return $this->dirty;
  }

  public function asArray() {
    return array(
      "name" => $this->name,
      "id" => $this->id,
      "createdAt" => $this->created_at->format(\DateTime::ISO8601),
      "updatedAt" => $this->updated_at->format(\DateTime::ISO8601)
    );
  }
}
