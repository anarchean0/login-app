<?php

namespace LoginApp\Views;

require_once __ROOT__ . '/system/HTTPResponse.php';
require_once __ROOT__ . '/system/HTTPError.php';
require_once __ROOT__ . '/app/models/User.php';
require_once __ROOT__ . '/app/models/AuthToken.php';

use LoginApp\System\HTTPResponse;
use LoginApp\System\HTTPError;
use LoginApp\Models\User;
use LoginApp\Models\AuthToken;

class UserResource {
  public function register($req) {
    $data = $req->getJSON();

    $user = new User($data->name);
    $user->set_password($data->password);

    try {
      $req->getContext('userDao')->save($user);
      $user = $req->getContext('userDao')->get_by_id($user->get_id());
    } catch (\PDOException $ex) {
      throw new HTTPError("Invalid user information. " .
          "The name could already exist", 400);
    }

    return (new HTTPResponse())->setBody($user->asArray())->setStatus(201);
  }

  public function login($req) {
    $data = $req->getJSON();
    $dao = $req->getContext('userDao');
    $authFail = new HTTPError('Invalid user name or password', 403);

    $user = $dao->get_by_name($data->name);
    if (!$user) {
      throw $authFail;
    }

    if ($user->verify_password($data->password)) {
      $dao->save($user);
      $token = AuthToken::make($user);

      return HTTPResponse::make()->setBody(array(
        "token" => $token->buildToken()
      ));
    }

    throw $authFail;
  }

  public function info($req) {
    $user = $this->checkAuth($req);

    return HTTPResponse::make()->setBody($user->asArray());
  }

  public function changepass($req) {
    $user = $this->checkAuth($req);
    $data = $req->getJSON();
    $dao = $req->getContext("userDao");

    if (!$user->verify_password($data->currentPassword)) {
      throw new HTTPError("Invalid password", 403);
    }

    $user->set_password($data->newPassword);
    $dao->save($user);

    return HTTPResponse::make()->setStatus(200);
  }

  public function delete($req) {
    $user = $this->checkAuth($req);
    $data = $req->getJSON();
    $dao = $req->getContext("userDao");

    if (!$user->verify_password($data->password)) {
      throw new HTTPError("Invalid password", 403);
    }

    $dao->delete($user);

    return HTTPResponse::make()->setStatus(203);
  }

  private function checkAuth($req) {
    if (!($user = $req->getContext("user"))) {
      throw new HTTPError("You must be logged access this resource", 403);
    }

    return $user;
  }
}
