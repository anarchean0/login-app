<?php

namespace LoginApp\Filters;

require_once __ROOT__ . "/system/interfaces.php";

use LoginApp\System\Filter;

class DatabaseFilter implements Filter {
  private $daoClasses;

  public function __construct($daoClasses) {
    $this->daoClasses = $daoClasses;
  }

  public function handleRequest($req) {
    $pdo = new \PDO(\DBCONFIG['url'], \DBCONFIG['username'], \DBCONFIG['password']);
    $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    $req->setContext("_pdo", $pdo);

    foreach ($this->daoClasses as $var => $class) {
      $req->setContext($var, new $class($pdo));
    }
  }
}
