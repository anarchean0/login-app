package tes.logon.web;

import android.content.Context;
import android.nfc.FormatException;
import android.widget.TableRow;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import tes.logon.models.UserInfo;

public class LoginService {
    private static final String HOST = "localhost";
    private static final String BASE_URL = "http://" + HOST + ":8097/";
    private static final String TAG = "LOGIN_SERVICE_TAG";

    private String mAuthToken;
    private RequestQueue mQueue;

    private static LoginService mInstance;

    public interface CompletionHandler<T> {
        void onSuccess(T value);
        void onError(Throwable ex);
    }

    public LoginService(Context context) {
        this.mAuthToken = null;
        this.mQueue = Volley.newRequestQueue(context);
    }

    public void setQueue(RequestQueue queue) {
        this.mQueue = queue;
    }

    public RequestQueue getQueue() {
        return this.mQueue;
    }

    public void register(String userName, String password, CompletionHandler<Void> callback) {
        try {
            final JSONObject obj = new JSONObject()
                    .put("name", userName)
                    .put("password", password);

            final Response.Listener<JSONObject> okHandler = (resp) -> {
                callback.onSuccess(null);
            };
            final Response.ErrorListener errHandler = (error) -> {
                callback.onError(error);
            };

            final String url = BASE_URL + "register";

            Request<?> jsonRequest = new JsonObjectRequest(
                    Request.Method.POST, url, obj, okHandler, errHandler).setTag(TAG);

            this.mQueue.add(jsonRequest);
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void login(String userName, String password, CompletionHandler<Void> callback) {
        try {
            final JSONObject obj = new JSONObject()
                    .put("name", userName)
                    .put("password", password);

            final Response.Listener<JSONObject> okHandler = (response) -> {
                try {
                    mAuthToken = response.getString("token");
                    callback.onSuccess(null);
                } catch (JSONException ex) {
                    callback.onError(new RuntimeException("Token is not a string"));
                }
            };

            final Response.ErrorListener errorHandler = (error) -> {
                callback.onError(error);
            };

            mQueue.add(new JsonObjectRequest(Request.Method.POST, BASE_URL + "login",
                    obj, okHandler, errorHandler).setTag(TAG));
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void userInfo(CompletionHandler<UserInfo> callback) throws IllegalStateException {
        checkForAuthToken();

        final Response.Listener<JSONObject> okHandler = (response) -> {
            try {
                UserInfo info = new UserInfo();
                info.setId(response.getInt("id"));
                info.setName(response.getString("name"));
                info.setCreatedAt(DateTime.parse(response.getString("createdAt")));
                info.setUpdatedAt(DateTime.parse(response.getString("updatedAt")));
                callback.onSuccess(info);
            } catch (JSONException ex) {
                callback.onError(ex);
            }
        };

        final Response.ErrorListener errorHandler = (error) -> {
            callback.onError(error);
        };

        try {
            Request<?> request = new JsonObjectRequest(Request.Method.GET,
                    BASE_URL + "self", null, okHandler, errorHandler)
                    .setTag(TAG);

            request.getHeaders().put("Authorization", "Token " + mAuthToken);

            mQueue.add(request);
        } catch (AuthFailureError ex) {
            callback.onError(ex);
        }
    }

    public void changePassword(String current, String next, CompletionHandler<Void> callback) {
        checkForAuthToken();

        final Response.Listener<JSONObject> success = (obj) -> {
            callback.onSuccess(null);
        };

        final Response.ErrorListener error = callback::onError;

        try {
            final Request<?> req = new JsonObjectRequest(Request.Method.PUT,
                    BASE_URL + "self/changepass",
                    new JSONObject()
                            .put("currentPassword", current)
                            .put("newPassword", next),
                    success, error).setTag(TAG);

            req.getHeaders().put("Authorization", "Token " + mAuthToken);
            mQueue.add(req);
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        } catch (AuthFailureError ex) {
            callback.onError(ex);
        }
    }

    public void deleteUser(String password, CompletionHandler<Void> callback) {
        checkForAuthToken();

        final Response.Listener<JSONObject> success = (obj) -> {
            callback.onSuccess(null);
        };

        final Response.ErrorListener error = callback::onError;

        try {
            final Request<?> req = new JsonObjectRequest(Request.Method.DELETE,
                    BASE_URL + "self",
                    new JSONObject()
                            .put("password", "password"),
                    success,
                    error
            ).setTag(TAG);

            req.getHeaders().put("Authorization", "Token " + mAuthToken);

            mQueue.add(req);
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        } catch (AuthFailureError ex) {
            callback.onError(ex);
        }
    }

    private void checkForAuthToken() {
        if (mAuthToken == null) {
            throw new IllegalStateException("You can only get information of a logged user");
        }
    }

    public static void makeApplicationInstance(Context applicationContext) {
        if (mInstance != null) {
            destroyApplicationInstance();
        }

        mInstance = new LoginService(applicationContext);
    }

    public static void destroyApplicationInstance() {
        mInstance.getQueue().cancelAll(TAG);
        mInstance.setQueue(null);
        mInstance = null;
    }

    public static LoginService getApplicationInstance() {
        return mInstance;
    }
}
