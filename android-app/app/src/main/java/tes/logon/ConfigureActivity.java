package tes.logon;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class ConfigureActivity extends AppCompatActivity {
    private TextView textViewEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure);
    }

    public void EditUser(View v){
        Intent intent = new Intent(ConfigureActivity.this, CadastroActivity.class);

        intent.putExtra("_EMAIL_", (String) intent.getStringExtra("_EMAIL_"));

        startActivity(intent);
    }

    public void sair(View v){
        AlertDialog alert = new AlertDialog.Builder(this).create();
        alert.setTitle("Excluir");
        alert.setMessage("Desejar Excluir sua conta?");
        alert.show();

        Intent intent = new Intent(ConfigureActivity.this, MainActivity.class);

        startActivity(intent);
    }
}
