package tes.logon.models;

import org.joda.time.*;

public class UserInfo {
    private Integer mId;
    private String mName;
    private DateTime mCreatedAt;
    private DateTime mUpdatedAt;

    public UserInfo() {
        this(null, null, null, null);
    }

    public UserInfo(Integer id, String name, DateTime createdAt, DateTime updatedAt) {
        this.mId = id;
        this.mName = name;
        this.mCreatedAt = createdAt;
        this.mUpdatedAt = updatedAt;
    }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public DateTime getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(DateTime mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public DateTime getUpdatedAt() {
        return mUpdatedAt;
    }

    public void setUpdatedAt(DateTime mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }
}
